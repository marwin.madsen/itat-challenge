# ITAT Challenge 2024

Mit erfolgreich bestandener Challenge können Sie Ihr Klausurergebnis um 0,3 Notenpunkte verbessern, sofern die Klausur grundsätzlich bestanden ist (4,0 oder besser)!
- Die Challenge ist freiwillig
- Team bis zu 3 Studierende (1-3 möglich)
- Alle sitzen in einem Boot (Gruppe besteht oder nicht)

## Aufgabe

![UEFA_EM_Logo](https://upload.wikimedia.org/wikipedia/de/thumb/5/5c/Logo_Fu%C3%9Fball_Europameisterschaft_2024.svg/363px-Logo_Fu%C3%9Fball_Europameisterschaft_2024.svg.png)
![Paris_Olympia_Logo](https://upload.wikimedia.org/wikipedia/en/thumb/d/d1/2024_Summer_Olympics_logo.svg/422px-2024_Summer_Olympics_logo.svg.png)

Entwickeln Sie ein Tool, dass Prof. Barth eine Übersicht mindestens einer Sportart (Olympia, z.B. 100 Meter-Lauf) oder des Turniers (EM 2024) gibt:
- Tabellen
- Mannschaften, Sportler*innen
- Rankings etc.

Die teilnehmenden Sportler*innen (Olympia) bzw. Mannschaften (EM) können bereits von Ihnen implementiert werden.

Die Ergebnisse müssen von Prof. Barth eingetragen werden können.

Die resultierenden Standings, Tabellen, Statistiken sollen von Ihrer Software generiert und dargestellt werden.

Zusätzlich soll Ihre Software Statistiken generieren, wie hoch die Siegwahrscheinlichkeit eines/r Sportler*in oder eines Teams auf Basis des Verlaufs ist.

Diese Aufgabenstellung ist bewusst etwas offener gestaltet, um Ihnen Gegstaltungsfreiraum zu gewähren:
- Erstellen Sie ein Konzept
- Überlegen Sie sich auch, welchen Unique Selling Point ihre Lösung bieten kann
- Setzen Sie ihr Konzept unter Zuhilfenahme der Vorlesungsinhalte (OOP, (Smart) Pointer, Datenstrukturen) um

## Anforderungen
- Saubere Nutzung von git (branching, commit messages, merging) in einem private Repository auf der KIT GitLab Instanz mit dem Namen "ITAT_Challenge_2024_<Projektname>"
- Arbeit in einer Gruppe von bis zu 3 Personen ist erlaubt
- Das Programm muss für die Prüfer kompilierbar sein ohne zusätzliche Hardware- oder Softwareanforderungen (also keine Einbindung von Bibliotheken, die zusätzlich installiert werden müssten)
  - Ausnahme ist die Nutzung von [Qt6](https://wiki.qt.io/Einstieg_in_die_Programmierung_mit_Qt6) als Framework zur Erstellung einer grafischen Benutzeroberfläche (graphical user interface, GUI), falls das gewünscht ist (Command-Line Ausgaben sind für Bestehen vollkommen ausreichend)
  - ansonsten nur die C++ Standard Libraries 
  - C++17
  - g++ Compiler
- Das Programm muss eine sinnvolle Verwendung der Vorlesungs- und Übungskonzepte aufweisen (d.h. es werden OOP, (smart) Pointer und Datenstrukturen erwartet)
- Ausfürliche Dokumentation in der README.md 
  - Projektname
  - Namen und Matrikelnummern der Ersteller
  - textuelle Beschreibung 
    - Was macht das Programm und wie ist es zu bedienen? 
    - Was der Unique Selling Point (USP) der vorgestellten Lösung? --> Was macht ihr Projekt besonders?
  - UML-Modelle mit zumindest einem Klassendiagramm


## Teilnahme

1. Erstellen Sie ein GitLab-Repository (auf gitlab.kit.edu gehostet) mit folgender Namenskonvention an:  "ITAT_Challenge_2024_PROJEKTNAME"
2. Fügen Sie, neben Ihren Teamkolleg:innen, Marcel Auer (marcel.auer) und Marwin Madsen (marwin.madsen) bis zum **28.06.2024** zu ihrem Repository hinzu. 
3. Erstellen Sie in ihrem Repository bis zum **16.08.2024** einen Release, der mit __Abgabe__ getagt ist (nur der Stand zum Release wird betrachtet). Nähere Infos zum Erstellen von Releases: [GitLab - Releases](https://docs.gitlab.com/ee/user/project/releases/)

# Beispiele und Hinweise: 
## UML Klassendiagramm
UML Klassendiagramme können sehr einfach z.B. mit [DrawIO](https://app.diagrams.net/) erstellt werden: 

<img src="img/UML.png" alt="uml" width="500"/>


Oder alternativ direkt in Markdown mit [Mermaid](https://mermaid.js.org/):

```mermaid
classDiagram
    class Person {
        -name: String
        -geld: double
        +Person(pName: String)
        +geldErhoehen(pBetrag: double)
        +geldVermindern(pBetrag: double): boolean
        +fahren(pKm: double): boolean
        +tanken(pLiter: double): boolean
        +setTankstelle(pTankstelle: Tankstelle)
    }
    class Tankstelle {
        -preisProLiter: double
        -name: String
        +Tankstelle(pName: String)
        +setPreis(pPreis: double)
        +getPreis(): double
        +tanken(pLiter: double): double
    }
    class Roller {
        -kmStand: double
        -tankInhalt: double
        -maxTank: double
        +Roller()
        +fahren(pKm: double): boolean
        +tanken(pLiter: double): boolean
    }
    Person --> "1" Roller : meinRoller
    Person --> "0..1" Tankstelle : die Tankstelle
```

## Projekt-Dokumentation
Beschreiben Sie hier, wie ihre Software zu nutzen ist. 
- Wie erfolgen die Eingaben?
- Für statistische Auswertung der Ergebnisse: Wie erfolgt die Berechung?
- Wie könnte ihr Projekt noch erweitert werden?

Als Inspriation für die Funktionsbeschreibung bzw. Anwendungsbeschreibung: [Mermaid - Github](https://github.com/mermaid-js/mermaid)

## .gitignore
In diesem Repository ist eine Datei mit dem Namen ___.gitignore___ hinterlegt. Wir raten ihnen diese Datei in der obersten Ebene (dort, wo auch README.md liegt) ihrem Projekt hinzuzufügen. 

Die in dieser Datei aufgelisteten Dateiformate werden dann von Git nicht mehr beachtet. Das verhindert, dass Dateien getrackt werden, welche automatisch beim Kompilieren erstellt werden und in ihrem Repository keinen Mehrwert darstellen würden. 

Nähere Infos: [Git - gitignore](https://git-scm.com/docs/gitignore)

